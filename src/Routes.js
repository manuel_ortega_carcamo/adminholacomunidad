import React from 'react'
import {Switch, Route} from 'react-router-dom'

import EmptyPage from './pages/empty-page'
import LoginPage from './pages/loginPage'
import ForgotPasswordPage from './pages/forgotPasswordPage'



const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <LoginPage />
      </Route>
      <Route exact path="/forgot-password" component={ForgotPasswordPage} />

      <Route component={EmptyPage} />
    </Switch>
  )
}
export default Routes
