import React from 'react'
import {
  FiToggleLeft,
  FiList,
  FiActivity,
  FiCalendar,
  FiStar,
  FiDroplet,
  FiGrid,
  FiClock,
  FiCopy,
  FiUser,
  FiPieChart,
  FiMap,
  FiCompass,
  FiHelpCircle,
  FiShoppingCart,
  FiHome
} from 'react-icons/fi'

const initialState = [
  {
    title: '',
    items: [
      {
        url: '/dashboard',
        icon: <FiCompass size={20} />,
        title: 'Inicio',
        items: []
      },
    ]
  },
  {
    title: 'Modulos',
    items: [
      {
        url: '/',
        icon: <FiCopy size={20} />,
        title: 'Noticias',
        badge: {
          color: 'bg-indigo-500 text-white',
          text: 7
        },
        items: [
          {
            url: '/contact-us-1',
            title: 'Nueva Noticia',
            items: []
          },
          {
            url: '/contact-us-1',
            title: 'Listado de Noticias',
            items: []
          },
          {
            url: '/subscribe',
            title: 'Pendientes de aprobación',
            items: []
          }
        ]
      },
      {
        url: '/',
        icon: <FiUser size={20} />,
        title: 'Usuarios',
        items: [
          {
            url: '/user-profile',
            title: 'Nuevo usuario',
            items: []
          },
          {
            url: '/social-feed',
            title: 'Empresas',
            items: []
          }
        ]
      },
    ]
  },
  {
    title: 'TRAZABILIDAD',
    items: [
      {
        url: '/dashboard',
        icon: <FiCompass size={20} />,
        title: 'Mapa usuarios',
        items: []
      },
    ]
  },
]

export default function navigation(state = initialState, action) {
  switch (action.type) {
    default:
      return state
  }
}
